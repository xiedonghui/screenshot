#include "controlwidget.h"
#include "ui_controlwidget.h"
#include <QPoint>
#include "OE/OEasyScreenshot"
#include <QDebug>
#include <QApplication>
#include <QClipboard>

ControlWidget::ControlWidget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::ControlWidget)
{
    ui->setupUi(this);
    connectSignalAndSlot();

}

void ControlWidget::connectSignalAndSlot()
{
    connect(ui->cancelBtn, SIGNAL(clicked()), this, SLOT(cancelBtn_slot()));
    connect(ui->saveBtn, SIGNAL(clicked()), this, SLOT(saveBtn_slot()));
    connect(ui->finishBtn, SIGNAL(clicked()), this, SLOT(finishBtn_slot()));


}
//撤销截图按钮
void ControlWidget::cancelBtn_slot()
{
    qDebug()<<"退出截图";
    if(screen){
        screen->quitScreenshot();
    }
}
//保存截图按钮
void ControlWidget::saveBtn_slot()
{
    qDebug()<<"保存截图";
    if(screen){
        screen->onSaveScreenOther();
    }
}

//完成按钮将截图保存到剪贴板
void ControlWidget::finishBtn_slot()
{
    qDebug()<<"完成截图";
    if(screen){
        screen->onSaveScreen();
    }
}


//保存Screen类的引用
void ControlWidget::setScreenQuote(OEScreen *screen)
{
    this->screen = screen;
    qDebug()<<"获得对象";
}

ControlWidget::~ControlWidget()
{
    delete ui;
}

void ControlWidget::on_drawLineBtn_clicked()
{

//    screen->drawline();
}



void ControlWidget::on_textEditBtn_clicked()
{
//    screen->textedit();
}

void ControlWidget::on_rectangleBtn_clicked()
{
//    screen->drawrectang();
}

void ControlWidget::on_drawRoundBtn_clicked()
{
//    screen->drawround();
}

void ControlWidget::on_arrowBtn_clicked()
{
//    screen->drawarrow();
}

void ControlWidget::on_mosaicBtn_clicked()
{
    //
}

void ControlWidget::on_returneditBtn_clicked()
{
    //
}

